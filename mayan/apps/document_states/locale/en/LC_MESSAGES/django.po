# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-14 23:46-0400\n"
"PO-Revision-Date: 2017-06-23 05:43+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: English (http://www.transifex.com/rosarior/mayan-edms/"
"language/en/)\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:44 queues.py:8
msgid "Document states"
msgstr "Document states"

#: apps.py:69 apps.py:76
msgid "Current state of a workflow"
msgstr "Current state of a workflow"

#: apps.py:70
msgid "Return the current state of the selected workflow"
msgstr "Return the current state of the selected workflow"

#: apps.py:77
msgid ""
"Return the completion value of the current state of the selected workflow"
msgstr ""
"Return the completion value of the current state of the selected workflow"

#: apps.py:92 models.py:38 models.py:95 models.py:155
msgid "Label"
msgstr "Label"

#: apps.py:95 models.py:35
msgid "Internal name"
msgstr "Internal name"

#: apps.py:99
msgid "Initial state"
msgstr "Initial state"

#: apps.py:100 apps.py:110 apps.py:120 apps.py:126
msgid "None"
msgstr "None"

#: apps.py:104
msgid "Current state"
msgstr "Current state"

#: apps.py:108 apps.py:135 models.py:302
msgid "User"
msgstr "User"

#: apps.py:114
msgid "Last transition"
msgstr "Last transition"

#: apps.py:118 apps.py:131
msgid "Date and time"
msgstr "Date and time"

#: apps.py:124 apps.py:151 models.py:107
msgid "Completion"
msgstr "Completion"

#: apps.py:138 forms.py:52 links.py:85 models.py:300
msgid "Transition"
msgstr "Transition"

#: apps.py:142 forms.py:55 models.py:303
msgid "Comment"
msgstr "Comment"

#: apps.py:147
msgid "Is initial state?"
msgstr "Is initial state?"

#: apps.py:155 models.py:159
msgid "Origin state"
msgstr "Origin state"

#: apps.py:159 models.py:163
msgid "Destination state"
msgstr "Destination state"

#: links.py:15 links.py:38 links.py:95 models.py:77 views.py:134 views.py:413
msgid "Workflows"
msgstr "Workflows"

#: links.py:20
msgid "Create workflow"
msgstr "Create workflow"

#: links.py:25 links.py:46 links.py:63
msgid "Delete"
msgstr "Delete"

#: links.py:29 models.py:42
msgid "Document types"
msgstr "Document types"

#: links.py:33 links.py:50 links.py:67
msgid "Edit"
msgstr "Edit"

#: links.py:41
msgid "Create state"
msgstr "Create state"

#: links.py:54 links.py:105
msgid "States"
msgstr "States"

#: links.py:58
msgid "Create transition"
msgstr "Create transition"

#: links.py:71
msgid "Transitions"
msgstr "Transitions"

#: links.py:77 queues.py:12
msgid "Launch all workflows"
msgstr "Launch all workflows"

#: links.py:81
msgid "Detail"
msgstr "Detail"

#: links.py:90
msgid "Workflow documents"
msgstr "Workflow documents"

#: links.py:99
msgid "State documents"
msgstr "State documents"

#: models.py:32
msgid ""
"This value will be used by other apps to reference this workflow. Can only "
"contain letters, numbers, and underscores."
msgstr ""
"This value will be used by other apps to reference this workflow. Can only "
"contain letters, numbers, and underscores."

#: models.py:76 models.py:93 models.py:153 models.py:181
msgid "Workflow"
msgstr "Workflow"

#: models.py:99
msgid ""
"Select if this will be the state with which you want the workflow to start "
"in. Only one state can be the initial state."
msgstr ""
"Select if this will be the state with which you want the workflow to start "
"in. Only one state can be the initial state."

#: models.py:101
msgid "Initial"
msgstr "Initial"

#: models.py:105
msgid ""
"Enter the percent of completion that this state represents in relation to "
"the workflow. Use numbers without the percent sign."
msgstr ""
"Enter the percent of completion that this state represents in relation to "
"the workflow. Use numbers without the percent sign."

#: models.py:113
msgid "Workflow state"
msgstr "Workflow state"

#: models.py:114
msgid "Workflow states"
msgstr "Workflow states"

#: models.py:174
msgid "Workflow transition"
msgstr "Workflow transition"

#: models.py:175
msgid "Workflow transitions"
msgstr "Workflow transitions"

#: models.py:184
msgid "Document"
msgstr "Document"

#: models.py:279 models.py:294
msgid "Workflow instance"
msgstr "Workflow instance"

#: models.py:280
msgid "Workflow instances"
msgstr "Workflow instances"

#: models.py:297
msgid "Datetime"
msgstr "Datetime"

#: models.py:309
msgid "Workflow instance log entry"
msgstr "Workflow instance log entry"

#: models.py:310
msgid "Workflow instance log entries"
msgstr "Workflow instance log entries"

#: models.py:314
msgid "Not a valid transition choice."
msgstr "Not a valid transition choice."

#: models.py:320
msgid "Workflow runtime proxy"
msgstr "Workflow runtime proxy"

#: models.py:321
msgid "Workflow runtime proxies"
msgstr "Workflow runtime proxies"

#: models.py:327
msgid "Workflow state runtime proxy"
msgstr "Workflow state runtime proxy"

#: models.py:328
msgid "Workflow state runtime proxies"
msgstr "Workflow state runtime proxies"

#: permissions.py:7
msgid "Document workflows"
msgstr "Document workflows"

#: permissions.py:10
msgid "Create workflows"
msgstr "Create workflows"

#: permissions.py:13
msgid "Delete workflows"
msgstr "Delete workflows"

#: permissions.py:16
msgid "Edit workflows"
msgstr "Edit workflows"

#: permissions.py:19
msgid "View workflows"
msgstr "View workflows"

#: permissions.py:25
msgid "Transition workflows"
msgstr "Transition workflows"

#: permissions.py:28
msgid "Execute workflow tools"
msgstr "Execute workflow tools"

#: serializers.py:22
msgid "Primary key of the document type to be added."
msgstr "Primary key of the document type to be added."

#: serializers.py:37
msgid ""
"API URL pointing to a document type in relation to the workflow to which it "
"is attached. This URL is different than the canonical document type URL."
msgstr ""
"API URL pointing to a document type in relation to the workflow to which it "
"is attached. This URL is different than the canonical document type URL."

#: serializers.py:116
msgid "Primary key of the destination state to be added."
msgstr "Primary key of the destination state to be added."

#: serializers.py:120
msgid "Primary key of the origin state to be added."
msgstr "Primary key of the origin state to be added."

#: serializers.py:218
msgid ""
"API URL pointing to a workflow in relation to the document to which it is "
"attached. This URL is different than the canonical workflow URL."
msgstr ""
"API URL pointing to a workflow in relation to the document to which it is "
"attached. This URL is different than the canonical workflow URL."

#: serializers.py:227
msgid "A link to the entire history of this workflow."
msgstr "A link to the entire history of this workflow."

#: serializers.py:259
msgid ""
"Comma separated list of document type primary keys to which this workflow "
"will be attached."
msgstr ""
"Comma separated list of document type primary keys to which this workflow "
"will be attached."

#: serializers.py:319
msgid "Primary key of the transition to be added."
msgstr "Primary key of the transition to be added."

#: views.py:53
#, python-format
msgid "Workflows for document: %s"
msgstr "Workflows for document: %s"

#: views.py:77
#, python-format
msgid "Detail of workflow: %(workflow)s"
msgstr "Detail of workflow: %(workflow)s"

#: views.py:101
#, python-format
msgid "Document \"%s\" transitioned successfully"
msgstr ""

#: views.py:110
msgid "Submit"
msgstr "Submit"

#: views.py:112
#, python-format
msgid "Do transition for workflow: %s"
msgstr "Do transition for workflow: %s"

#: views.py:164
msgid "Available document types"
msgstr "Available document types"

#: views.py:165
msgid "Document types assigned this workflow"
msgstr "Document types assigned this workflow"

#: views.py:175
#, python-format
msgid "Document types assigned the workflow: %s"
msgstr "Document types assigned the workflow: %s"

#: views.py:214 views.py:498
#, python-format
msgid "States of workflow: %s"
msgstr "States of workflow: %s"

#: views.py:232
#, python-format
msgid "Create states for workflow: %s"
msgstr "Create states for workflow: %s"

#: views.py:308
#, python-format
msgid "Transitions of workflow: %s"
msgstr "Transitions of workflow: %s"

#: views.py:321
#, python-format
msgid "Create transitions for workflow: %s"
msgstr "Create transitions for workflow: %s"

#: views.py:351
msgid "Unable to save transition; integrity error."
msgstr "Unable to save transition; integrity error."

#: views.py:440
#, python-format
msgid "Documents with the workflow: %s"
msgstr "Documents with the workflow: %s"

#: views.py:461
#, python-format
msgid "Documents in the workflow \"%s\", state \"%s\""
msgstr "Documents in the workflow \"%s\", state \"%s\""

#: views.py:512
msgid "Launch all workflows?"
msgstr "Launch all workflows?"

#: views.py:519
msgid "Workflow launch queued successfully."
msgstr "Workflow launch queued successfully."
