# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Marco Camplese <marco.camplese.mc@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-14 23:47-0400\n"
"PO-Revision-Date: 2016-09-24 09:51+0000\n"
"Last-Translator: Marco Camplese <marco.camplese.mc@gmail.com>\n"
"Language-Team: Italian (http://www.transifex.com/rosarior/mayan-edms/language/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:18 links.py:31 links.py:35 permissions.py:7 views.py:36
msgid "Events"
msgstr "Eventi"

#: apps.py:27
msgid "Timestamp"
msgstr "Timestamp"

#: apps.py:29
msgid "Actor"
msgstr "Attore"

#: apps.py:31
msgid "Verb"
msgstr "Verbo"

#: classes.py:23
#, python-brace-format
msgid "Unknown or obsolete event type: {0}"
msgstr "Tipo di evento obsoleto o sconosciuto: {0}"

#: models.py:13
msgid "Name"
msgstr "Nome "

#: models.py:17
msgid "Event type"
msgstr "Tipo evento"

#: models.py:18
msgid "Event types"
msgstr "Tipi evento"

#: permissions.py:9
msgid "Access the events of an object"
msgstr "Accedere agli eventi di un oggetto"

#: views.py:29 views.py:84
msgid "Target"
msgstr "Destinazione"

#: views.py:69
#, python-format
msgid "Events for: %s"
msgstr "Eventi per: %s"

#: views.py:92
#, python-format
msgid "Events of type: %s"
msgstr "Eventi di tipo: %s"
